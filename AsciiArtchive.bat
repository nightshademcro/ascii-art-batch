title #
MODE 1000
@echo off
cls
cls
echo                       (
echo                         )     (
echo                  ___...(-------)-....___
echo              .-""       )    (          ""-.
echo        .-'``'I-._             )         _.-I
echo       /  .--.I   `""---...........---""`   I
echo      /  /    I                             I
echo      I  I    I                             I
echo       \  \   I                             I
echo        `\ `\ I                             I
echo          `\ `I                             I
echo          _/ /\                             /
echo         (__/  \                           /
echo      _..---""` \                         /`""---.._
echo   .-'           \                       /          '-.
echo  :               `-.__             __.-'              :
echo  :                  ) ""---...---"" (                 :
echo   '._               `"--...___...--"`              _.'
echo     \""--..__                              __..--""/
echo      '._     """----.....______.....----"""     _.'
echo         `""--..,,_____            _____,,..--""`
echo                       `"""----"""`
timeout 1 >nul
timeout 1 >nul
cls
echo                       (
echo                         )     (
echo                  ___...(-------)-....___
echo              .-""       )    (          ""-.
echo        .-'``'I-._             )         _.-I
echo       /  .--.I   `""---...........---""`   I
echo      /  /    I                             I
echo      I  I    I                             I
echo       \  \   I                             I
echo        `\ `\ I                             I
echo          `\ `I                             I
echo          _/ /\                             /
echo         (__/  \                           /
echo      _..---""` \                         /`""---.._
echo   .-'           \                       /          '-.
echo  :               `-.__             __.-'              :
echo  :                  ) ""---...---"" (                 :
echo   '._               `"--...___...--"`              _.'
echo     \""--..__                              __..--""/
echo      '._     """----.....______.....----"""     _.'
echo         `""--..,,_____            _____,,..--""`
echo                       `"""----"""`
timeout 1 >nul
cls
echo      /  /    I                             I
echo      I  I    I                             I
echo       \  \   I                             I
echo        `\ `\ I                             I
echo          `\ `I                             I
echo          _/ /\                             /
echo         (__/  \                           /
echo      _..---""` \                         /`""---.._
echo   .-'           \                       /          '-.
echo  :               `-.__             __.-'              :
echo  :                  ) ""---...---"" (                 :
echo   '._               `"--...___...--"`              _.'
echo     \""--..__                              __..--""/
echo      '._     """----.....______.....----"""     _.'
echo         `""--..,,_____            _____,,..--""`
echo                       `"""----"""`
ping 127.0.0.1 -n 1 -w 500> nul
cls
echo   .-'           \                       /          '-.
echo  :               `-.__             __.-'              :
echo  :                  ) ""---...---"" (                 :
echo   '._               `"--...___...--"`              _.'
echo     \""--..__                              __..--""/
echo      '._     """----.....______.....----"""     _.'
echo         `""--..,,_____            _____,,..--""`
echo                       `"""----"""`
ping 127.0.0.1 -n 1 -w 500> nul
cls
echo      '._     """----.....______.....----"""     _.'
echo         `""--..,,_____            _____,,..--""`
echo                       `"""----"""`
ping 127.0.0.1 -n 1 -w 500> nul
cls
:menus
echo Welcome to the Ascii library.
echo Type the number of your choice.
echo Contents:
echo (1) - Animals
echo (2) - Art and design
set /p choice1=
if %choice1% == 1 goto :Animals
if %choice1% == 2 goto :art
cls
goto :menus
:phones
cls
echo Art by lgbeard
echo                     ___________
echo                    /.---------.\`-._
echo                   //          II    `-._
echo                   II `-._     II        `-._
echo                   II     `-._ II            `-._
echo                   II    _____ II`-._            \
echo             _..._ II   I __ ! II    `-._        I
echo           _/     \II   .'  I~~II        `-._    I
echo       .-``     _.`II  /   _I~~II    .----.  `-._I
echo      I      _.`  _II  I  I23I II   / :::: \    \
echo      \ _.--`  _.` II  I  I56I II  / ::::: I    I
echo       I   _.-`  _.II  I  I79I II  I   _..-'   /
echo       _\-`   _.`O II  I  I_   II  I::I        I
echo     .`    _.`O `._II  \    I  II  I::I        I
echo  .-`   _.` `._.'  II   '.__I--II  I::I        \
echo `-._.-` \`-._     II   I ":  !II  I  '-.._    I
echo          \   `--._II   I_:"___II  I ::::: I   I
echo           \  /\   II     ":":"II   \ :::: I   I
echo            \(  `-.II       .- II    `.___/    /
echo            I    I II   _.-    II              I
echo            I    / \\.-________\\____.....-----'
echo            \    -.      \ I         I
echo             \     `.     \ \        I 
echo  __________  `.    .'\    \I        I\  _________
echo LGB            `..'   \    I        I \          
echo                 \\   .'    I       /  .`.
echo                 I \.'      I       I.'   `-._
echo                  \     _ . /       \_\-._____)
echo                   \_.-`  .`'._____.'`.
echo                     \_\-I             I
echo                          `._________.'
pause >nul
cls
echo                    ..--""""----..                 
echo                 .-"   ..--""""--.j-.              
echo              .-"   .-"        .--.""--..          
echo           .-"   .-"       ..--"-. \/    ;         
echo        .-"   .-"_.--..--""  ..--'  "-.  :         
echo      .'    .'  /  `. \..--"" __5_     \ ;         
echo     :.__.-"    \  /      4 .' ( )"-. 6 Y          
echo     ;           ;:        ( )     ( ).  \         
echo   .':          /::       :            \  \        
echo .'.-"\._   _.-" ; ;    3 ( )    .-.  ( ) 7\       
echo  "    `."""  .j"  :      :      \  ;    ;  \      
echo    bug /"""""/     ;      ( )    "" :.( )   \     
echo       /\    /      :    2  \         \`.: 8_ \    
echo      :  `. /        ;       `( )     (\/ :" \ \   
echo       \   `.        :       1 "-.(_)_.'9  t-'  ;  
echo        \    `.       ;            0       ..--":  
echo         `.    `.     :              ..--""     :  
echo           `.    "-.   ;       ..--""           ;  
echo             `.     "-.:_..--""            ..--"   
echo               `.      :             ..--""        
echo                 "-.   :       ..--""              
echo                    "-.;_..--""
pause >nul
cls
echo                  ________
echo                .' /  /   )
echo               /  /##/   /I
echo              /  `--'   / I
echo             /__ __ __ /  I
echo            //_//_//_//  / __
echo           //_//_//_//  /  \`.___ Listening end
echo          //_//_//_//  /
echo         //_//_//_//  /__
echo        /         /  / \`.___ Buttons
echo       /   .-.   /  /
echo      /   /#/   /  /
echo     /   `-'   /  /__
echo    / .====.  /  / \`.___ Speaking end
echo   I`--------'  /
echo    \   ,     .'__
echo     `-//----'  \`.___ Disconnect button
echo      //
echo     //
echo    (())
echo    (())
echo    (()) __
echo    (()) \`.___ The usual messy stuff (much longer IRL)
echo    (())
echo    (())             I
echo    (())             I
echo    (())            /I
echo    (())        .--.II
echo    (())        I  III __
echo    (())       .I  III \`.___ Wall
echo    (())     .'.I  III
echo    (())___.'.' `--'II
echo     `------'       \I
echo                     I
echo VK                  I
pause >nul
cls
echo                 .-.
echo                 I I
echo                 I I
echo                 I I
echo                 I I
echo                 I I
echo                 I I
echo                 I I
echo                 I I
echo      _.--"""""""--;_
echo     //             \\
echo     II   .-"""-.   II
echo     II  /  ...  \  II
echo     II I  :::::  I II
echo     II  \  '''  /  II
echo     II   '-...-'   II
echo     I/.-----------.\I
echo     II.-"""""""""-.II
echo     III___________III
echo     II[__][___][__]II
echo     II=== ===== ===II
echo     I\  ===   ===  /I
echo     I `'""""""""""` I
echo     I[TALK] === === I
echo     I.---..---..---.I
echo     II_1_II_2_II_3_II
echo     I.---..---..---.I
echo     II_4_II_5_II_6_II
echo     I.---..---..---.I
echo     II_7_II_8_II_9_II
echo     I.---..---..---.I
echo     II_*_II_0_II_#_II
echo     I____ _____ ____I
echo     I==== ===== ====I
echo     I====  ___  ====I
echo     I   .'`   `'.   I
echo jgs I  /  .:::.  \  I
echo     \ '  {CASIO}  ' /
echo      `--.........--'
pause >nul
cls
goto :menus
:animals
cls
echo   ,-.       _,---._ __  / \
echo  /  )    .-'       `./ /   \
echo (  (   ,'            `/    /I
echo  \  `-"             \'\   / I
echo   `.              ,  \ \ /  I
echo    /`.          ,'-`----Y   I
echo   (            ;        I   '
echo   I  ,-.    ,-'         I  /
echo   I  I (   I        hjw I /
echo   )  I  \  `.___________I/
echo   `--'   `--'
pause >nul
cls
echo           .__....._             _.....__,
echo             .": o :':         ;': o :".
echo             `. `-' .'.       .'. `-' .'
echo               `---'             `---'
echo                                                                                                                                                                                                                                                 x
echo     _...----...      ...   ...      ...----..._
echo  .-'__..-""'----    `.  `"`  .'    ----'""-..__`-.
echo '.-'   _.--"""'       `-._.-'       '"""--._   `-.`
echo '  .-"'                  :                  `"-.  `
echo   '   `.              _.'"'._              .'   `
echo         `.       ,.-'"       "'-.,       .'
echo           `.                           .'
echo             `-._                   _.-'
echo                 `"'--...___...--'"`
pause >nul
cls
echo                     I 
echo ____________    __ -+-  ____________ 
echo \_____     /   /_ \ I   \     _____/
echo  \_____    \____/  \____/    _____/
echo   \_____                    _____/
echo      \___________  ___________/
echo                /____\
pause >nul
cls
echo                  _  _
echo                 ( \/ )
echo          .---.   \  /   .-"-. 
echo         /   6_6   \/   / 4 4 \
echo         \_  (__\       \_ v _/
echo         //   \\        //   \\
echo        ((     ))      ((     ))
echo jgs=====""===""========""===""=======
echo           III            III
echo            I              I
pause >nul
cls
echo          .   ,
echo        '. '.  \  \
echo       ._ '-.'. `\  \
echo         '-._; .'; `-.'. 
echo        `~-.; '.       '.
echo         '--,`           '.
echo            -='.          ;
echo  .--=~~=-,    -.;        ;
echo  .-=`;    `~,_.;        /
echo `  ,-`'    .-;         I
echo    .-~`.    .;         ;
echo     .;.-   .-;         ,\
echo       `.'   ,=;     .-'  `~.-._
echo        .';   .';  .'      .'   '-.
echo          .\  ;  ;        ,.' _  a',
echo         .'~";-`   ;      ;"~` `'-=.)
echo       .' .'   . _;  ;',  ;
echo       '-.._`~`.'  \  ; ; :
echo            `~'    _'\\_ \\_ 
echo                  /=`^^=`""/`)-.
echo             jgs  \ =  _ =     =\
echo                   `""` `~-. =   ;
pause >nul
cls
echo 
echo         _
echo        / \      _-'
echo      _/I  \-''- _ /
echo __-' { I          \
echo     /             \
echo     /       "o.  Io }
echo     I            \ ;
echo                   ',
echo        \_         __\
echo          ''-_    \.//
echo            / '-____'
echo           /
echo         _'
echo       _-'
pause >nul
cls
echo                             _
echo                           .' `'.__
echo                          /      \ `'"-,
echo         .-''''--...__..-/ .     I      \
echo       .'               ; :'     '.  a   I
echo      /                 I :.       \     =\
echo     ;                   \':.      /  ,-.__;.-;`
echo    /I     .              '--._   /-.7`._..-;`
echo   ; I       '                I`-'      \  =I
echo   I/\        .   -' /     /  ;         I  =/
echo   (( ;.       ,_  .:I     I /     /\   I =I
echo    ) / `\     I `""`;     / I    I /   / =/
echo      I ::I    I      \    \ \    \ `--' =/
echo     /  '/\    /       )    I/     `-...-`
echo    /    I I  `\    /-'    /;
echo    \  ,,/ I    \   D    .'  \
echo jgs `""`   \  nnh  D_.-'L__nnh
echo             `"""`
pause >nul
cls
goto :menus
:art
echo    ______________________________
echo  / \                             \.
echo I   I                            I.
echo  \_ I                            I.
echo     I                            I.
echo     I                            I.
echo     I                            I.
echo     I                            I.
echo     I                            I.
echo     I                            I.
echo     I                            I.
echo     I                            I.
echo     I                            I.
echo     I                            I.
echo     I                            I.
echo     I   _________________________I___
echo     I  /                            /.
echo     \_/dc__________________________/.
pause >nul
cls
echo                      ,---.           ,---.
echo                     / /"`.\.--"""--./,'"\ \
echo                     \ \    _       _    / /
echo                      `./  / __   __ \  \,'
echo                       /    /_O)_(_O\    \
echo                       I  .-'  ___  `-.  I
echo                    .--I       \_/       I--.
echo                  ,'    \   \   I   /   /    `.
echo                 /       `.  `--^--'  ,'       \
echo              .-"""""-.    `--.___.--'     .-"""""-.
echo .-----------/         \------------------/         \--------------.
echo I .---------\         /----------------- \         /------------. I
echo I I          `-`--`--'                    `--'--'-'             I I
echo I I                                                             I I
echo I I                                                             I I
echo I I                                                             I I
echo I I                                                             I I
echo I I                                                             I I
echo I I                                                             I I
echo I I                                                             I I
echo I I                                                             I I
echo I I                                                             I I
echo I I                                                             I I
echo I I                                                             I I
echo I I                                                             I I
echo I I_____________________________________________________________I I
echo I_________________________________________________________________I
echo                    )__________I__I__________(
echo                   I            II            I
echo                   I____________II____________I
echo                     ),-----.(      ),-----.(
echo                   ,'   ==.   \    /  .==    `.
echo                  /            )  (            \
echo                  `==========='    `==========='  hjw
pause >nul
cls
echo                         ,
echo                     ..--'_I
echo                    /  \.' :              ;--- .
echo                    I \/ - I      .\      I`./  `,     ..---'I
echo           ;--- .   \_/.-./     .'I \     ; _\..-. _  /  \.'`:
echo           I`./  `,,' `.  I _  / \'/ I   _.\_ I  .' `,I \/ - I
echo           ; _\.I _I_.,-._.' \ I  I/ '.-'\./ `._.-,._I_ /-  / .-''.
echo            \   ,' /  \-.I \  I\ \I // I /I  / I.-/  \ `,--' I_ / /`-.,
echo             `'-I  \_.,`-'_/_.' `._'.-.'  `._\_`-',._/  I    \ \''-'`/
echo           .-.._\   / \_.';   I ,' `.  I _I   ;`._/ \   /    _'.__.-'
echo        _ I  .' `'-.I    /   ; _I_.,-._.' \;   \    I.-.-.,/` `\
echo       / `._.-,._I_ \._.'`'-',' /  \-.I \  I`-'``._./ /`  / _  I`-.
echo      I  / I.-/  \ `,        I  \_.,`-'_/_.'         I   ;.` \ /   \
echo .--- `._\_`-',._/  I        \   / \_.';'  I         .`-/'.-,'`'\  I
echo I`./ I   ;`._/ \   /         '-.I    /   ;          I  \ I`-\ _/_.'
echo ; _\.I;   \    I.-'             `._.'`'-'            \_.`'`-'' I   _.'
echo  \   '.`-'``._./                                        I  `,_.' '\.'I
echo   `'--'    _ .-.                                       _'.-.' / I /_ ;
echo     .''-.,' `.  I _                                  ,' `.  II_  '  /
echo ,.-'\ \ _I_.,-._./ \                                _I_.,-._.' \._-`
echo  \''-',' /  \-.I \  I                             /` /  \-.I \  I_/-.,
echo   `-._I  \_.,`-'_/_.'                             I  \_.,`-'_/_.'\-'/
echo       \   / \_.';   I                             \   / \_.';   I__.'
echo    .'_ '-I     /   ; .-.                        .-.'-.I    /   ; _`.
echo   / _.'\  \._.'`'-''`.  I _                  _ I  .' `,\._.'`'-''._ ;
echo   I.'\  .'      _I_.,-._.' \                / `._.-,._I_     `.  \'.I
echo   '---'`     _,' /  \-.I \  I      _       I  / I.-/  \ `,-''. `'---'
echo            .'_I  \_.,`-'_/_.'    /` `\,.-. `._\_`-',._/  I / /`-.,
echo           / _.\   / \_.';   I .-'I  _ \   \I   ;`._/ \   /\''-'`/
echo           I.'\ '-I     /   ; /   \ / '.;   I;   \    I.-`'.__.-'
echo           '---'` .\._.'`'-'  I  /`'`,-.`\-'. `-'``._./
echo                 /_ .'\    .''`._\_ /-'I /  I   /\`_`.mx
echo                : .' \;,.-'\ \ _`I ``-'`'._/-. I I'._ ;
echo                I/ I ,' \''-''/ /`._,'  I/ /I \'.  \'.I
echo                '---'    `-.__.'    _'-' I  I\ . `'---'
echo                                   / /I ;\  /  I
echo                                   I  I\ . '.\/
echo                                   \ /.\ I   `
echo                                    '.I /
echo                                      '`
pause >nul
cls
echo            .e$.                           z$$e.d$$$.      z$b   z
echo          d$" .d                        .$$" d"F ^*$$$e  z$" $ .$
echo        e$P   $%                       d$P ."  F    "$$"d$  .e$"
echo       $$F                           .$$"  F  J       "$$z$$$"
echo     .$$"   .$"3   .$""  .$P $$  $$ 4$$"  $  4"       $$  .
echo    .$$F   d$  4  d$ d$ z$" J$%    4$$"   $.d"       $$  ."
echo    $$P   $$ ".$z$$  ^ z$" .$P    .$$F              $$" ."
echo   $$$   d$F  J $$F   z$$  $$ .   $$$   ze     .c  J$F z  .e.ze
echo  4$$F   $$  4" $$   z$$  $$"."  d$$  d$" $  z$" $ $$ @  $$".$F
echo  $$$   4$$.d" 4$$ .$3$$.$$$e%   $$P J$P  P d$*  %$$$"  $$  $$
echo echo  $$$    $$*    $$$" ^$$"'$$    4$$% $$" . d$" "$"$$   $$  $$
echo  $$$                           $$$  "   P4$P  z 4$F  $$" J$% %
echo  '$$c          .e$$$$$$$$e     $$$F    $ $$  z" $$  $$$ 4$$ P
echo   "$$b.   .e$*"     "$$$$$$$   '$$$c.dP  $$$$"  $$$"$$$$$$$P
echo     "*$$$*"           "          *$$*"   "$*    "$" ^$* "$"
echo Gilo94'
pause >nul
cls
echo                           .,aad88888888888baa,.
echo                      ,ad8888888888888888888888888ba,.
echo                  ,ad888888888888888888888888888888888ba,
echo               ,ad888888888P""'            """Y88888888888ba.
echo             ,d88888888P""                       ""Y888888888ba
echo           a888888888"                               ""Y88888888b,
echo         ,888888888b,                                   ""Y8888888b,
echo        d888888888888b,                                    "Y8888888b,
echo      ,8888888' "888888b,                                    "Y8888888b
echo     ,888888"     "Y88888b,                                    "Y888888b
echo    ,888888'        "Y88888b,                                    "888888b
echo   ,888888'     a,  8a"Y88888b,                                   `888888a
echo  ,888888'      `8, `8) "Y88888b,                  ,adPPRg,        `888888,
echo  888888'        8)  ]8   "Y88888b,            ,ad888888888b        Y88888b
echo d88888P        ,8' ,8'     "Y88888b,      ,gPPR888888888888        `888888,
echo 888888'       ,8' ,8'        "Y88888b,,ad8""   `Y888888888P         )88888)
echo 888888        8)  8)           "Y888888"        (8888888""          (88888)
echo 888888        8,  8,          ,ad8Y88888b,      d888""              d88888)
echo 888888        `8, `8,     ,ad8""   "Y88888b,,ad8""                  888888)
echo 888888         `8, `" ,ad8""         "Y88888b"                     ,888888'
echo Y88888,           ,gPPR8b           ,ad8Y88888b,                   d888888
echo `88888b          dP:::::Yb      ,ad8""   "Y88888b,                ,888888P
echo  888888,         8):::::(8  ,ad8""         "Y88888b,              d888888'
echo  `888888,        Yb:;;;:d888""               "Y88888b,           d888888P
echo   Y888888,        "8ggg8P"                     "Y88888b,       ,d888888P
echo    Y88888b,                                      "Y88888b,    ,8888888"
echo     Y88888b,                                       "Y88888b, d8888888"
echo      Y888888,                                        "Y888888888888P'
echo       "888888b,                                        "8888888888"
echo         Y888888b,                                     ,888888888"
echo           Y8888888ba,                              ,a888888888"
echo             "Y88888888ba,._                   .,ad888888888P"
echo                "Y88888888888bbaa,,_____,,aadd88888888888""
echo                    "Y8888888888888888888888888888888""  Normand
echo                        ""Y888888888888888888888P""      Veilleux
echo                               """"""""""""""
echo 3116
pause >nul
cls


